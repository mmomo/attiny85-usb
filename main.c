#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include "usbdrv.h"

#define USB_LED_OFF 0
#define USB_LED_ON  1

#define USB_DATA_OUT   2
#define USB_DATA_WRITE 3
#define USB_DATA_IN    4

#define LED_PIN (1 << PB3)

static uchar replyBuf[16] = "Hello, USB!";
static uchar dataReceived = 0, dataLength = 0;

USB_PUBLIC uchar usbFunctionSetup(uchar data[8]) {
  usbRequest_t *rq = (void *)data;

  switch (rq->bRequest) {
  case USB_LED_ON:
    PORTB |= LED_PIN;
    return 0;
    
  case USB_LED_OFF:
    PORTB &= ~LED_PIN;
    return 0;

  case USB_DATA_OUT:
    usbMsgPtr = replyBuf;
    return sizeof(replyBuf);

  case USB_DATA_WRITE:
    replyBuf[7] = rq->wValue.bytes[0];
    replyBuf[8] = rq->wValue.bytes[1];
    replyBuf[9] = rq->wIndex.bytes[0];
    replyBuf[10] = rq->wIndex.bytes[1];
    return 0;

  case USB_DATA_IN:
    dataLength = (uchar)rq->wLength.word;
    dataReceived = 0;

    if (dataLength > sizeof(replyBuf)) {
      dataLength = sizeof(replyBuf);
    }
    return USB_NO_MSG;
  }
  
  return 0;
}

USB_PUBLIC uchar usbFunctionWrite(uchar *data, uchar len) {
  uchar i;
  for (i = 0; dataReceived < dataLength && i < len; i++, dataReceived++) {
    replyBuf[dataReceived] = data[i];
  }
  
  return (dataReceived == dataLength);
}

#define abs(x) ((x) > 0 ? (x) : (-x))

// Called by V-USB after device reset
void hadUsbReset() {
    int frameLength, targetLength = (unsigned)(1499 * (double)F_CPU / 10.5e6 + 0.5);
    int bestDeviation = 9999;
    uchar trialCal, bestCal, step, region;

    // do a binary search in regions 0-127 and 128-255 to get optimum OSCCAL
    for(region = 0; region <= 1; region++) {
        frameLength = 0;
        trialCal = (region == 0) ? 0 : 128;
        
        for(step = 64; step > 0; step >>= 1) { 
            if(frameLength < targetLength) // true for initial iteration
                trialCal += step; // frequency too low
            else
                trialCal -= step; // frequency too high
                
            OSCCAL = trialCal;
            frameLength = usbMeasureFrameLength();
            
            if(abs(frameLength-targetLength) < bestDeviation) {
                bestCal = trialCal; // new optimum found
                bestDeviation = abs(frameLength -targetLength);
            }
        }
    }

    OSCCAL = bestCal;
}

int main() {
  uchar i;

  DDRB = LED_PIN;
  
  wdt_enable(WDTO_1S);

  usbInit();

  usbDeviceDisconnect();
  for (i = 0; i < 250; i++) {
    wdt_reset();
    _delay_ms(2);
  }

  usbDeviceConnect();
  sei();

  while(1) {
    wdt_reset();
    usbPoll();
  }

  return 0;
}


